#!/bin/bash

git config --global user.email "slatty@uottawa.ca"
git config --global user.name "Sadiki Latty"
git pull
git add .
commit=$1
if [[ -z $commit ]]; then
    commit="Turn [skip ci]"
fi
git commit -m "$commit"
git push origin $BITBUCKET_BRANCH
