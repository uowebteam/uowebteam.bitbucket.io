 function nameFunction() {
	event.preventDefault();
    $('form').fadeOut(500);
	$('.wrapper').addClass('form-success');
	
	 setTimeout(function(){
		window.location.replace("play.php");
	 }, 500);

    localStorage.clear();
	localStorage.setItem("first_player_name", first_player.value);
	localStorage.setItem("second_player_name", second_player.value);
	localStorage.setItem("count", 1);
	jQuery.ajax({
    type: "POST",
    url: 'play.php',
    dataType: 'json',
    data: {functionname: 'names', arguments: [first_player.value,second_player.value, branch.value]},

    success: function (obj, textstatus) {
                  if( !('error' in obj) ) {
                      names = obj.result;
					  console.log(names);
                  }
                  else {
                      console.log(obj.error);
                  }
            }
	});
}

if (localStorage.getItem("first_player_name") != null) {
	var check = parseInt(localStorage.getItem("count"));
	if (check % 2 == 0) {
		document.getElementById('player_id').innerHTML = localStorage.getItem("second_player_name") + "'s Move (x)";
	}
	else {
		document.getElementById('player_id').innerHTML = localStorage.getItem("first_player_name") + "'s Move (o)";
	}

	localStorage.setItem("count", 	check + 1);
	
}

 function playFunction() {
	event.preventDefault();
	/*$('form').fadeOut(500);
	$('.wrapper').addClass('form-success');*/
 
	/*setTimeout(function(){
		window.location.replace("play.php");
	}, 5000);*/
	
  console.log(player);
  console.log(branch);
  
  console.log(row.value);
  console.log(column.value);
  console.log(player.value);
  console.log(branch.value);
  
  console.log("MOVE SENT: " + new Date().toLocaleTimeString());
	jQuery.ajax({
    type: "POST",
    url: '/itfair/move.php',
    dataType: 'json',
    data: {functionname: 'move', arguments: [row.value,column.value,player.value,branch.value]},
    
    success: function (obj, textstatus) {
      console.log("SUCCESS " + new Date().toLocaleTimeString());
      if( !('error' in obj) ) {
        move = obj.result;
        console.log(move);
        row.value = '';
        column.value = '';
        player.value = '';
      }
      else {
        console.log(obj.error);
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.log("ERROR " + new Date().toLocaleTimeString());
    },
    complete: function(data) {
      console.log("COMPLETE " + new Date().toLocaleTimeString());
      row.value = '';
      column.value = '';
      player.value = '';
    } 
	});	
}

 function resetFunction() {
	event.preventDefault();
	$('form').fadeOut(500);
	$('.wrapper').addClass('form-success');
	setTimeout(function(){
		window.location.replace("player_name.html");
	}, 1000);
	
	localStorage.clear();
	
	jQuery.ajax({
    type: "POST",
    url: 'move.php',
    dataType: 'json',
    data: {functionname: 'delete'},
	});	
}