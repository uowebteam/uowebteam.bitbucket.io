# Tic tac toe - IT FAIR 2018

![alt](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2018/May/23/2369188327-1-jbecart.bitbucket.io-logo_avatar.png)

Master: 

![Build status](https://img.shields.io/bitbucket/pipelines/jbecart/jbecart.bitbucket.io.svg)
![Pull request](https://img.shields.io/bitbucket/pr-raw/jbecart/jbecart.bitbucket.io.svg)
![Issues](https://img.shields.io/bitbucket/issues-raw/jbecart/jbecart.bitbucket.io.svg)
![Coverage](https://img.shields.io/coveralls/bitbucket/jbecart/jbecart.bitbucket.io.svg) 

Artifacts:
[Download](https://bitbucket.org/jbecart/jbecart.bitbucket.io/downloads/coverage-26.tar.gz)
